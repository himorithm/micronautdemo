package com.intothebasket.micronaut

import com.intothebasket.micronaut.controller.EmployeeClient
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class MicronautExampleTest extends Specification {

    @Inject
    EmployeeClient employeeClient

    void 'test create new Name'() {
        expect:
        null != employeeClient.CreateEmployee("Humppy")
    }

    void 'test search new name'() {
        expect:
        null != employeeClient.getEmployee("Humppy")
    }

}