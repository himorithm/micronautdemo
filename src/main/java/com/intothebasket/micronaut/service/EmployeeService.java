package com.intothebasket.micronaut.service;

import com.intothebasket.micronaut.model.Employee;
import com.intothebasket.micronaut.repository.EmployeeRepository;

import javax.inject.Singleton;

@Singleton
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee findFinest(String name) {
        return employeeRepository.findByName(name).stream().findFirst().orElse(
                Employee.builder().name("dummy").city("Dummy").build());
    }


    public Employee create(String name) {
        final Employee employee = Employee.builder().name(name).city(name).build();
        return employeeRepository.save(employee);
    }
}
