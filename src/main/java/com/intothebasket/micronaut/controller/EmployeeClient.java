package com.intothebasket.micronaut.controller;

import io.micronaut.http.client.annotation.Client;

@Client("/employee")
public interface EmployeeClient extends EmployeeApi {
}
