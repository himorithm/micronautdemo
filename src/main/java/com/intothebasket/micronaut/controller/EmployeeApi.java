package com.intothebasket.micronaut.controller;

import com.intothebasket.micronaut.model.Employee;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;

public interface EmployeeApi {

    @Get("/{name}")
    Employee getEmployee(@PathVariable String name);

    @Get("/create/{name}")
    Employee CreateEmployee(@PathVariable String name);
}
