package com.intothebasket.micronaut.controller;

import com.intothebasket.micronaut.model.Employee;
import com.intothebasket.micronaut.service.EmployeeService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;

@Controller("/employee")
public class EmployeeController implements EmployeeApi {

    private final EmployeeService service;

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @Get("/{name}")
    @Override
    public Employee getEmployee(@PathVariable String name) {
       return service.findFinest(name);
    }

    @Get("/create/{name}")
    @Override
    public Employee CreateEmployee(@PathVariable String name) {
        return service.create(name);
    }

}
